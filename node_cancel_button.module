<?php

/**
 * Implements hook_menu()
 */
function node_cancel_button_menu(){
  return array(
    'admin/config/user-interface/cancel' => array(
      'title' => 'Cancel',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('node_cancel_button_settings'),
      'access arguments' => array('administer contents'),
    ),
  );
}

function node_cancel_button_settings($form, &$form_state) {
  $node_type = array();
  $label = 'Cancel';
  $action = 'front';
  if (!empty(node_cancel_button_settings_get('node'))) {
   $node_type = node_cancel_button_settings_get('node');
  }
  $types = node_type_get_names();
  $form['node'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#description' => t('Enable cancel button for checked content types'),
    '#options' => $types,
    '#default_value' => $node_type,
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('Provide label text for Cancel button'),
    '#default_value' => $label,
  );
  $form['action'] = array(
    '#type' => 'select',
    '#title' => t('Action on cancel'),
    '#description' => t('Provide action to be taken on clicking cancel button'),
    '#options' => node_cancel_button_action_options(),
    '#default_value' => $action,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Callback for cancel button settings submit.
 */
function node_cancel_button_settings_submit($form, $form_state) {

  // Clear settings before save.
  node_cancel_button_settings_del();

  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {

    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }

    node_cancel_button_settings_set($key, $value);
  }

  drupal_set_message(t('Cancel button settings have been saved.'));
}


/**
 * Provides options for form action on cancel.
 */
function node_cancel_button_action_options() {
 return  array(
    'front' => 'Redirect to front page',
    'node_view' => 'Redirect to Node view page',
    'Destination' => 'Destination URL if set, Else to front page',
  );
}

/**
 * Get settings for cancel button.
 *
 * @param string $name
 *   (optional) setting name.
 *
 * @return mixed
 *   settings value.
 */
function node_cancel_button_settings_get($name = NULL) {

  $settings = variable_get('node_cancel_button_settings');
  if(!isset($settings[$name])) {
    return $settings;
  }

  return $settings[$name];
}

/**
 * Set settings for cancel button.
 *
 * @param string $name
 *   setting name.
 * @param (mixed) $value
 *   setting value.
 */
function node_cancel_button_settings_set($name, $value) {
  $settings = variable_get('node_cancel_button_settings');

  $settings[$name] = $value;

  variable_set('node_cancel_button_settings', $settings);
}

/**
 * Delete setting from cancel button.
 *
 * @param string $name
 *   (optional) setting name. If not set removes all the settings.
 * @param (mixed) $value
 *   setting value.
 */
function node_cancel_button_settings_del($name = NULL) {

  if(!$name) {
    variable_del('node_cancel_button_settings');
    return;
  }

  $settings = variable_get('node_cancel_button_settings');

  unset($settings[$name]);

  variable_set('node_cancel_button_settings', $settings);
}

/**
 * Checks is form needs cancel button.
 *
 * @param string $form_id
 *   Form id.
 * @param array $form
 *   Form array.
 *
 * @return boolean
 *   TRUE is form needs cancel button.
 */
function node_cancel_buttom_needed($form_id, $form) {
  if (node_cancel_button_needed_for_node_form($form_id, $form)) {
    return TRUE;
  }
}

/**
 * Checks is node form needs cancel button.
 *
 * @param string $form_id
 *   Form id.
 * @param array $form
 *   Form array.
 *
 * @return boolean
 *   TRUE is form needs cancel button.
 */
function node_cancel_button_needed_for_node_form($form_id, $form) {
  $node_type = '';
  if (isset($form['#node']->type) && isset($form['#node_edit_form'])) {
    $node_type = $form['#node']->type;
  }
  elseif (isset($form['type']) && !empty($form['#type']['#value']) && isset($form['#node_edit_form'])) {
    $node_type = $form['#type']['#value'];
  }
  if (!$node_type) {
    return;
  }
  $settings = node_cancel_button_settings_get('node');
  if (empty($settings[$node_type])) {
    return;
  }
  if ($form_id == $node_type . '_node_form') {
    return TRUE;
  }
}

/**
 * Implements hook_form_alter().
 */
function node_cancel_button_form_alter(&$form, &$form_state, $form_id) {
  if (node_cancel_buttom_needed($form_id, $form)) {
    node_cancel_button_add_to_form($form, $form_state);
  }
}

/**
 * Adds cancel button to the form
 *
 * @global string $base_url
 *   Global base url.
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function node_cancel_button_add_to_form(&$form, &$form_state) {
  // Get the referer.
  if (isset($_GET['destination'])) {
    $form_state['storage']['#referer'] = $_GET['destination'];
  }

  $form['actions']['cancel_button'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#weight' => 99,
    '#validate' => array('node_cancel_button_validate'),
    '#limit_validation_errors' => array(),
  );
}

/**
 * The "Cancel" action.
 * Handle different submit actions and make different redirects.
 */
function node_cancel_button_validate($form, &$form_state) {
  // Hide the error messages.
  drupal_get_messages('error');

  $redirect = '<front>';

  //if (arg(0) === 'node' && arg(1) === 'add') {
    //$redirect = 'node/add';
  //}
  if (arg(0) === 'node' && is_numeric(arg(1)) && arg(2) === 'edit') {
    $redirect = 'node/' . arg(1);
  }
  if (arg(0) === 'user' && is_numeric(arg(1)) && arg(2) === 'edit') {
    $redirect = 'user/' . arg(1);
  }

  if (isset($form_state['storage']['#referer'])) {
    $redirect = $form_state['storage']['#referer'];
  }
  elseif (isset($form['storage']['#referer'])) {
    $redirect = $form['storage']['#referer'];
  }

  drupal_goto($redirect);
}
